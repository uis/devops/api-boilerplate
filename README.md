# API Boilerplate

This repository contains a
[cookiecutter](https://github.com/audreyr/cookiecutter) template for API Projects in use by the
Devops Division of UIS

## Quickstart

```bash
$ pip install cookiecutter
$ cookiecutter https://gitlab.developers.cam.ac.uk/uis/devops/api-boilerplate.git
```

## Configuration

Cookie cutter will ask you for the following values:

* **project_name**: A human-readable name for the project. E.g. "UIS Coffee API".
* **project_slug**: A filename-safe version of the project name. Used to construct filenames
    for project metadata. By default, generated from project name. E.g., for the project name
    above, "uis_coffee-api".
* **project_repo**: Full URL of the repository of the project.
* **project_description**: A description of what this API does, used in the README and FastAPI
    `App` description (which populates the OpenAPI description).

## Testing

The repository is tested via the `tox` test runner. The tests include a basic
check that the template can generate output and that running tox in the
generated output succeeds.
