#!/usr/bin/env bash
#
# Wrapper script to run tox. Arguments are passed directly to tox.

# Exit on failure
set -e

# Change to this script's directory
cd "$( dirname "${BASH_SOURCE[0]}")"

# Execute tox runner, logging command used
set -x

# Force rebuild to make sure you are using latest code
docker-compose --project-name api_boilerplate --file tox-compose.yml build
# run tox service
exec docker-compose --project-name api_boilerplate --file ./tox-compose.yml run --rm tox $@
