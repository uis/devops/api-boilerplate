# Dockerfile for running tests locally
FROM python:3.8-slim

WORKDIR /usr/src/app

# Pre-install requirements to make re-build times smaller when developing.
ADD ./requirements.txt ./requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

ADD . .

