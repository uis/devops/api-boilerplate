# {{ cookiecutter.project_name }}

{{ cookiecutter.project_description }}

## Developer quickstart

Firstly, [install docker-compose](https://docs.docker.com/compose/install/).

Then, most tasks can be performed via the ``compose.sh`` script:

```bash
# Start development server
$ ./compose.sh development

# Start development server in background
$ ./compose.sh development up -d

# View logs
$ ./compose.sh development logs

# Stop the development server
$ ./compose.sh development down

# Start a 'production' configuration of the server
$ ./compose.sh production
```

## Running tests

Tests can be run using the `./test.sh` command:

```bash
# Run all PyTest tests and Flake8 checks
$ ./test.sh

# Run just PyTest
$ ./test.sh -e py3

# Run a single test file within PyTest
$ ./test.sh -e py3 -- tests/test_boilerplate_routes.py

# Run a single test file within PyTest with verbose logging
$ ./test.sh -e py3 -- tests/test_boilerplate_routes.py -vvv
```

## Technology

This API is built on [FastAPI](https://fastapi.tiangolo.com/) and packaged as a Docker image
expected to be deployed to the cloud.

## Settings

Settings can be defined within `./src/settings.py` within the `Settings` class, following the
[example given by FastAPI](https://fastapi.tiangolo.com/advanced/settings/?h=settings#create-the-settings-object).
Settings are loaded from `EXTERNAL_SETTINGS_URLS`, allowing them to be held in GCP storage buckets,
secret manager secrets or local files. When running locally the development and production
configurations are set up to use the secrets defined in `compose/dev-settings.yml` and
`compose/prod-settings.yml` respectively.

Settings are validated when the API starts up, so any missing or badly defined secrets will cause
the API to fail to startup.
