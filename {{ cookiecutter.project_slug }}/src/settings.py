import os
import deepmerge
import yaml
import geddit
from pydantic import BaseSettings


class Settings(BaseSettings):
    # define application settings here with types - see
    # https://fastapi.tiangolo.com/advanced/settings/#pydantic-settings
    # These settings are populated from EXTRA_SETTINGS_URLS, using `load_settings`
    pass


def load_settings() -> dict:
    """
    A function that parses the `EXTRA_SETTINGS_URLS` env var, merges the configuration files
    retrieved from each URL, and returns the resulting Settings instance.

    This will throw if the Settings is not valid.
    """
    extra_settings = {}
    extra_settings_urls = os.getenv("EXTRA_SETTINGS_URLS")

    if extra_settings_urls:
        for url in os.getenv("EXTRA_SETTINGS_URLS").split(','):
            extra_settings = deepmerge.always_merger.merge(
                extra_settings, yaml.safe_load(geddit.geddit(url))
            )
    return Settings(**(extra_settings or {}))
