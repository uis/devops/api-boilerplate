from fastapi import FastAPI

from . import dependencies

"""
Load settings immediately in order to validate settings provided by `EXTRA_SETTINGS_URLS`.

"""
dependencies.get_settings()


app = FastAPI(
    title='{{ cookiecutter.project_name }}',
    description='{{ cookiecutter.project_description }}',
    version='v1alpha1',
)


@app.get("/")
def read_root():
    return {"hello": "world"}


@app.get("/healthy", include_in_schema=False)
def read_healthy():
    return {"ok": True}
