from functools import lru_cache

from . import settings


@lru_cache()
def get_settings() -> settings.Settings:
    """
    Gets an instance of the current application's settings by calling `load_settings`.

    """
    return settings.load_settings()
