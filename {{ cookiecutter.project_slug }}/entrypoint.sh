#!/usr/bin/env sh
exec gunicorn -b "0.0.0.0:$PORT" -w 4 -k uvicorn.workers.UvicornWorker --chdir ./src --timeout 60 src.main:app
