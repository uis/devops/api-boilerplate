from unittest import TestCase
from fastapi.testclient import TestClient

from src.main import app


class BoilerplateRoutesTestCase(TestCase):
    client = TestClient(app)

    def test_read_root(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'hello': 'world'})

    def test_read_healthy(self):
        response = self.client.get('/healthy')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'ok': True})
