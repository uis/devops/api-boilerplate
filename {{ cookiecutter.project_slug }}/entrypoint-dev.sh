#!/usr/bin/env sh
exec uvicorn src.main:app --reload --host "0.0.0.0" --port "$PORT" --reload-dir ./src